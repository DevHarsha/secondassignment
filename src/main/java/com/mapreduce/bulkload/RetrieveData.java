package com.mapreduce.bulkload;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

public class RetrieveData {

    static final String TABLE_NAME = "person";

    private static final byte[] CF_NAME = Bytes.toBytes("information");

    private static final byte[][] QUAL_BYTES = {"name".getBytes(), "age".getBytes(),"company".getBytes(),
            "building_code".getBytes(),"phone_number".getBytes(),
            "address".getBytes()};

    public static void main(String[] args) throws IOException, Exception{


        Configuration config = HBaseConfiguration.create();
        Connection connection = ConnectionFactory.createConnection(config);
        Admin admin = connection.getAdmin();

        Table table = connection.getTable(TableName.valueOf(TABLE_NAME));

        Get g = new Get(Bytes.toBytes("row0"));

        Result result = table.get(g);

        // Printing the values
        String person_name = Bytes.toString(result.getValue(CF_NAME,QUAL_BYTES[0]));
        String person_age = Bytes.toString(result.getValue(CF_NAME,QUAL_BYTES[1]));
        String person_company = Bytes.toString(result.getValue(CF_NAME,QUAL_BYTES[2]));
        String person_building_code = Bytes.toString(result.getValue(CF_NAME,QUAL_BYTES[3]));
        String person_phone_number = Bytes.toString(result.getValue(CF_NAME,QUAL_BYTES[4]));
        String person_address = Bytes.toString(result.getValue(CF_NAME,QUAL_BYTES[5]));

        System.out.println(person_name.getClass().getSimpleName() + " " + person_address+ " " +person_age+ " " +person_company
                + " " +person_building_code+ " " +person_phone_number);


    }
}
