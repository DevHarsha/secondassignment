package com.mapreduce.wordcount;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;

import java.io.IOException;

public class WordCountDriver extends Configured implements Tool {

    private Path INPUT;
    private Path OUTPUT;

    public WordCountDriver(Path input,Path output){
        this.INPUT = input;
        this.OUTPUT = output;
    }

    @Override
    public int run(String[] args) {

        Configuration conf = new Configuration();
        Job job = getJob(conf);

        boolean success = false;
        try {
            success = job.waitForCompletion(true);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return success?0:1;

    }

    private Job getJob(Configuration conf)  {

        Job job = null;
        try {

            job = Job.getInstance(conf, "word count");
            job.setJarByClass(WordCountDriver.class);
            job.setMapperClass(WordCountMapper.class);
            job.setCombinerClass(WordCountReducer.class);
            job.setReducerClass(WordCountReducer.class);
            job.setOutputKeyClass(Text.class);
            job.setOutputValueClass(IntWritable.class);

            FileInputFormat.addInputPath(job, INPUT);
            FileOutputFormat.setOutputPath(job, OUTPUT);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return job;
    }

}
