import com.mapreduce.bulkload.BulkLoadDriver;
import com.mapreduce.wordcount.WordCountDriver;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.util.Bytes;

public class Main {

    public static void main(String args[]) {

        Path bulkload_input = new Path("/user/DevHarsha/output/person_10.csv");
        Path bulkload_output = new Path("/user/DevHarsha/bulkload");
        TableName TABLE_NAME = TableName.valueOf("person2");
        byte[] CF_NAME = Bytes.toBytes("information");


        Path wordcount_input = new Path("/user/DevHarsha/MR/superheroes.txt");
        Path wordcount_output = new Path("/user/DevHarsha/MR/output");

        BulkLoadDriver bulkLoadDriver = new BulkLoadDriver(bulkload_input,bulkload_output,TABLE_NAME,CF_NAME);
        WordCountDriver wordCountDriver = new WordCountDriver(wordcount_input,wordcount_output);

        bulkLoadDriver.run(args);
        wordCountDriver.run(args);

    }

}
